unit untAbout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActnList, jpeg, ExtCtrls, Buttons;

type
  TfrmAbout = class(TForm)
    imgAbout: TImage;
    ActionList1: TActionList;
    actClose: TAction;
    pnlAbout: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    lblAboutB: TLabel;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    Image6: TImage;
    Image7: TImage;
    Image8: TImage;
    imgAbNext: TImage;
    imgAbPre: TImage;
    imgAbHome: TImage;
    lblAbPre: TLabel;
    lblAbNext: TLabel;
    lblAbHome: TLabel;
    actNextPage: TAction;
    avtPrePage: TAction;
    imgIntrod: TImage;
    imgBack: TImage;
    lblBack: TLabel;
    procedure actCloseExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lblAboutBMouseEnter(Sender: TObject);
    procedure lblAboutBMouseLeave(Sender: TObject);
    procedure lblAboutBClick(Sender: TObject);
    procedure lblAbPreMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblAbPreMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblAbNextMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblAbNextMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblAbHomeMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblAbHomeMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure lblAbPreClick(Sender: TObject);
    procedure lblAbNextClick(Sender: TObject);
    procedure lblAbHomeClick(Sender: TObject);
    procedure lblBackMouseEnter(Sender: TObject);
    procedure lblBackMouseLeave(Sender: TObject);
    procedure lblBackClick(Sender: TObject);
  private
    procedure AbImaging(n: Byte);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAbout: TfrmAbout;
  ImgNo: Byte;

implementation

{$R *.dfm}

procedure TfrmAbout.actCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmAbout.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  pnlAbout.SendToBack;
end;

procedure TfrmAbout.lblAboutBMouseEnter(Sender: TObject);
begin
  imgIntrod.Visible := True;
end;

procedure TfrmAbout.lblAboutBMouseLeave(Sender: TObject);
begin
  imgIntrod.Visible := False;
end;

procedure TfrmAbout.lblAboutBClick(Sender: TObject);
begin
  pnlAbout.BringToFront;
end;

procedure TfrmAbout.lblAbPreMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgAbPre.Visible := True;
end;

procedure TfrmAbout.lblAbPreMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  imgAbPre.Visible := False;
end;

procedure TfrmAbout.lblAbNextMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgAbNext.Visible := True;
end;

procedure TfrmAbout.lblAbNextMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  imgAbNext.Visible := False;
end;

procedure TfrmAbout.lblAbHomeMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgAbHome.Visible := True;
end;

procedure TfrmAbout.lblAbHomeMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  imgAbHome.Visible := False;
end;

procedure TfrmAbout.FormShow(Sender: TObject);
begin
  ImgNo := 0;
  AbImaging(1);
end;

procedure TfrmAbout.lblAbPreClick(Sender: TObject);
begin
  AbImaging(ImgNo - 1);
end;

procedure TfrmAbout.lblAbNextClick(Sender: TObject);
begin
  AbImaging(ImgNo + 1);
end;

procedure TfrmAbout.AbImaging(n: Byte);
begin
  if n < 1 then
    n := 1
  else if n > 8 then
    n := 8;
  if n = ImgNo then
    Exit;
  ImgNo := n;
  Image1.Visible := False;
  Image2.Visible := False;
  Image3.Visible := False;
  Image4.Visible := False;
  Image5.Visible := False;
  Image6.Visible := False;
  Image7.Visible := False;
  Image8.Visible := False;
  case ImgNo of
    1: Image1.Visible := True;
    2: Image2.Visible := True;
    3: Image3.Visible := True;
    4: Image4.Visible := True;
    5: Image5.Visible := True;
    6: Image6.Visible := True;
    7: Image7.Visible := True;
    8: Image8.Visible := True;
  end;
end;

procedure TfrmAbout.lblAbHomeClick(Sender: TObject);
begin
  AbImaging(1);
  Panel1.BringToFront;
end;

procedure TfrmAbout.lblBackMouseEnter(Sender: TObject);
begin
  imgBack.Visible := True;
end;

procedure TfrmAbout.lblBackMouseLeave(Sender: TObject);
begin
  imgBack.Visible := False;
end;

procedure TfrmAbout.lblBackClick(Sender: TObject);
begin
  actCloseExecute(Sender);
end;

end.
